/*
** YUNI's default license is the GNU Lesser Public License (LGPL), with some
** exclusions (see below). This basically means that you can get the full source
** code for nothing, so long as you adhere to a few rules.
**
** Under the LGPL you may use YUNI for any purpose you wish, and modify it if you
** require, as long as you:
**
** Pass on the (modified) YUNI source code with your software, with original
** copyrights intact :
**  * If you distribute electronically, the source can be a separate download
**    (either from your own site if you modified YUNI, or to the official YUNI
**    website if you used an unmodified version) – just include a link in your
**    documentation
**  * If you distribute physical media, the YUNI source that you used to build
**    your application should be included on that media
** Make it clear where you have customised it.
**
** In addition to the LGPL license text, the following exceptions / clarifications
** to the LGPL conditions apply to YUNI:
**
**  * Making modifications to YUNI configuration files, build scripts and
**    configuration headers such as yuni/platform.h in order to create a
**    customised build setup of YUNI with the otherwise unmodified source code,
**    does not constitute a derived work
**  * Building against YUNI headers which have inlined code does not constitute a
**    derived work
**  * Code which subclasses YUNI classes outside of the YUNI libraries does not
**    form a derived work
**  * Statically linking the YUNI libraries into a user application does not make
**    the user application a derived work.
**  * Using source code obsfucation on the YUNI source code when distributing it
**    is not permitted.
** As per the terms of the LGPL, a "derived work" is one for which you have to
** distribute source code for, so when the clauses above define something as not
** a derived work, it means you don't have to distribute source code for it.
** However, the original YUNI source code with all modifications must always be
** made available.
*/
#pragma once
#include "../static/if.h"
#ifdef YUNI_OS_WINDOWS
#	include "../system/windows.hdr.h"
#endif


// Determine if we must use a mutex or not
#if defined(YUNI_OS_WINDOWS) || YUNI_OS_GCC_VERSION >= 40102 || defined(YUNI_OS_CLANG)
#	if !defined(YUNI_OS_WINDOWS) && !defined(YUNI_HAS_SYNC_ADD_AND_FETCH)
#		define YUNI_ATOMIC_MUST_USE_MUTEX 1
#	else
#		define YUNI_ATOMIC_MUST_USE_MUTEX 0
#	endif
#else
#	define YUNI_ATOMIC_MUST_USE_MUTEX 1
#endif

#if YUNI_ATOMIC_MUST_USE_MUTEX == 1
#	define YUNI_ATOMIC_INHERITS  : public TP<Int<Size,TP> >
#else
#	define YUNI_ATOMIC_INHERITS
#endif



namespace Yuni
{
namespace Atomic
{

	// Forward declaration
	template<int Size, template<class> class TP> class Int;


} // namespace Atomic
} // namespace Yuni



namespace Yuni
{
namespace Private
{
namespace AtomicImpl
{


	template<int ThreadSafe, class C>
	struct ThreadingPolicy final
	{
		#if YUNI_ATOMIC_MUST_USE_MUTEX == 1
		// If the class must be thread-safe, we have to provide a lock
		// mecanism to ensure thread-safety
		typedef typename Static::If<ThreadSafe,
			Policy::ObjectLevelLockable<C>, Policy::SingleThreaded<C> >::ResultType Type;
		# else
		// No lock is required, the operating system or the compiler already
		// provides all we need
		typedef Policy::SingleThreaded<C> Type;
		#endif

	}; // class ThreadingPolicy




	template<int ThreadSafe, class T>
	struct Volatile final
	{
		#if YUNI_ATOMIC_MUST_USE_MUTEX == 1
		// We have to use our own mutex, we don't care of the volatile keyword
		typedef T Type;
		# else
		// The operating system or the compiler already provides methods
		// to deal with atomic types. However the volatile keyword is
		// required to avoid dangerous optimizations by the compiler
		// when the class must be thread-safe (to avoid cache-optimisations
		// SMP processors for example)
		typedef typename Static::If<ThreadSafe, volatile T, T>::ResultType Type;
		#endif

	}; // class Volatile




	template<int Size>
	struct TypeFromSize final {};


	// bool
	template<> struct TypeFromSize<1> final
	{
		// We should use a signed 32 bits integer for boolean
		enum { size = 32 };
		typedef sint32 Type;
	};

	// Int16
	template<> struct TypeFromSize<16> final
	{
		// On OS X, there are only routines for int32_t and int64_t
		// With MinGW, it simply does not exist
		// It seems that the best solution is to use int32 everywhere
		enum { size = 32 };
		typedef sint32 Type;
	};

	// Int32
	template<> struct TypeFromSize<32> final
	{
		enum { size = 32 };
		typedef sint32 Type;
	};

	// Int64
	template<> struct TypeFromSize<64> final
	{
		enum { size = 64 };
		typedef sint64 Type;
	};




	// Thread-safe operations
	template<int Size, template<class> class TP>
	struct Operator final {};


	template<template<class> class TP>
	struct Operator<32, TP> final
	{
		template<class T>
		static typename Yuni::Atomic::Int<32,TP>::Type Increment(T& t)
		{
			#ifdef YUNI_OS_WINDOWS
			return ::InterlockedIncrement((LONG*)&t.pValue);
			# else
			#	if YUNI_ATOMIC_MUST_USE_MUTEX == 1
			typename T::ThreadingPolicy::MutexLocker locker(t);
			return (++t.pValue);
			#	else
			return __sync_add_and_fetch(&t.pValue, 1);
			#	endif
			#endif
		}

		template<class T>
		static typename Yuni::Atomic::Int<32,TP>::Type Increment(const T& t, typename T::ScalarType value)
		{
			#ifdef YUNI_OS_WINDOWS
			return InterlockedExchange((LONG*)&t.pValue, (LONG)(t.pValue + value));
			# else
			#	if YUNI_ATOMIC_MUST_USE_MUTEX == 1
			typename T::ThreadingPolicy::MutexLocker locker(t);
			return (t.pValue += value);
			#	else
			return __sync_add_and_fetch(&t.pValue, value);
			#	endif
			#endif
		}

		template<class T>
		static typename Yuni::Atomic::Int<32,TP>::Type Decrement(T& t)
		{
			#ifdef YUNI_OS_WINDOWS
			#   ifdef YUNI_OS_MINGW
			return ::InterlockedDecrement((LONG*)&t.pValue);
			#   else
			return _InterlockedDecrement((LONG*)&t.pValue);
			#   endif
			# else
			#	if YUNI_ATOMIC_MUST_USE_MUTEX == 1
			typename T::ThreadingPolicy::MutexLocker locker(t);
			return (--t.pValue);
			#	else
			return __sync_add_and_fetch(&t.pValue, -1);
			#	endif
			#endif
		}

		template<class T>
		static typename Yuni::Atomic::Int<32,TP>::Type Decrement(T& t, typename T::ScalarType value)
		{
			#ifdef YUNI_OS_WINDOWS
			return InterlockedExchange((LONG*)&t.pValue, (LONG)(t.pValue - value));
			# else
			#	if YUNI_ATOMIC_MUST_USE_MUTEX == 1
			typename T::ThreadingPolicy::MutexLocker locker(t);
			return (t.pValue -= value);
			#	else
			return __sync_add_and_fetch(&t.pValue, -value);
			#	endif
			#endif
		}

		template<class T>
		static void Zero(T& t)
		{
			#ifdef YUNI_OS_WINDOWS
			::InterlockedExchange((LONG*)&t.pValue, 0);
			# else
			#	if YUNI_ATOMIC_MUST_USE_MUTEX == 1
			typename T::ThreadingPolicy::MutexLocker locker(t);
			t.pValue = 0;
			#	else
			__sync_and_and_fetch(&t.pValue, 0);
			#	endif
			#endif
		}

		template<class T>
		static void Set(T& t, sint32 newvalue)
		{
			#ifdef YUNI_OS_WINDOWS
			::InterlockedExchange((LONG*)&t.pValue, newvalue);
			# else
			#	if YUNI_ATOMIC_MUST_USE_MUTEX == 1
			typename T::ThreadingPolicy::MutexLocker locker(t);
			t.pValue = newvalue;
			#	else
			__sync_synchronize();
			t.pValue = newvalue;
			#	endif
			#endif
		}

	}; // class Operator<32, TP>





	template<template<class> class TP>
	struct Operator<64, TP> final
	{
		template<class T>
		static typename Yuni::Atomic::Int<64,TP>::Type Increment(T& t)
		{
			#ifdef YUNI_OS_WINDOWS
			#	ifdef YUNI_OS_MINGW32
			YUNI_STATIC_ASSERT(false, AtomicOperator_NotImplementedWithMinGW);
			#	else
			return _InterlockedIncrement64((LONGLONG*)&t.pValue);
			#	endif
			# else
			#	if YUNI_ATOMIC_MUST_USE_MUTEX == 1
			typename T::ThreadingPolicy::MutexLocker locker(t);
			return (++t.pValue);
			#	else
			return __sync_add_and_fetch(&t.pValue, 1);
			#	endif
			#endif
		}

		template<class T>
		static typename Yuni::Atomic::Int<64,TP>::Type Increment(const T& t, typename T::ScalarType value)
		{
			#ifdef YUNI_OS_WINDOWS
			#	ifdef YUNI_OS_MINGW32
			YUNI_STATIC_ASSERT(false, AtomicOperator_NotImplementedWithMinGW);
			#	else
			return InterlockedExchange64((LONGLONG*)&t.pValue, (LONGLONG)(t.pValue + value));
			#   endif
			# else
			#	if YUNI_ATOMIC_MUST_USE_MUTEX == 1
			typename T::ThreadingPolicy::MutexLocker locker(t);
			return (t.pValue += value);
			#	else
			return __sync_add_and_fetch(&t.pValue, value);
			#	endif
			#endif
		}

		template<class T>
		static typename Yuni::Atomic::Int<64,TP>::Type Decrement(T& t)
		{
			#ifdef YUNI_OS_WINDOWS
			#	ifdef YUNI_OS_MINGW32
			YUNI_STATIC_ASSERT(false, AtomicOperator_NotImplementedWithMinGW);
			#	else
			return _InterlockedDecrement64((LONGLONG*)&t.pValue);
			#	endif
			# else
			#	if YUNI_ATOMIC_MUST_USE_MUTEX == 1
			typename T::ThreadingPolicy::MutexLocker locker(t);
			return (--t.pValue);
			#	else
			return __sync_add_and_fetch(&t.pValue, -1);
			#	endif
			#endif
		}

		template<class T>
		static typename Yuni::Atomic::Int<64,TP>::Type Decrement(T& t, typename T::ScalarType value)
		{
			#ifdef YUNI_OS_WINDOWS
			#	ifdef YUNI_OS_MINGW32
			YUNI_STATIC_ASSERT(false, AtomicOperator_NotImplementedWithMinGW);
			#	else
			return InterlockedExchange64((LONGLONG*)&t.pValue, (LONGLONG)(t.pValue - value));
			#   endif
			# else
			#	if YUNI_ATOMIC_MUST_USE_MUTEX == 1
			typename T::ThreadingPolicy::MutexLocker locker(t);
			return (t.pValue -= value);
			#	else
			return __sync_add_and_fetch(&t.pValue, -value);
			#	endif
			#endif
		}

		template<class T>
		static void Zero(T& t)
		{
			#ifdef YUNI_OS_WINDOWS
			#	ifdef YUNI_OS_MINGW32
			YUNI_STATIC_ASSERT(false, AtomicOperator_NotImplementedWithMinGW);
			#	else
			::InterlockedExchange64((LONGLONG*)&t.pValue, 0);
			#   endif
			# else
			#	if YUNI_ATOMIC_MUST_USE_MUTEX == 1
			typename T::ThreadingPolicy::MutexLocker locker(t);
			t.pValue = 0;
			#	else
			__sync_and_and_fetch(&t.pValue, 0);
			#	endif
			#endif
		}

		template<class T>
		static void Set(T& t, sint64 newvalue)
		{
			#ifdef YUNI_OS_WINDOWS
			#	ifdef YUNI_OS_MINGW32
			YUNI_STATIC_ASSERT(false, AtomicOperator_NotImplementedWithMinGW);
			#	else
			::InterlockedExchange64((LONGLONG*)&t.pValue, newvalue);
			#   endif
			# else
			#	if YUNI_ATOMIC_MUST_USE_MUTEX == 1
			typename T::ThreadingPolicy::MutexLocker locker(t);
			t.pValue = newvalue;
			#	else
			__sync_synchronize();
			t.pValue = newvalue;
			#	endif
			#endif
		}

	}; // class Operator<64, TP>





} // namespace AtomicImpl
} // namespace Private
} // namespace Yuni
